package com.joseva.appmenubutton

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import android.net.Uri

class MiAdaptador(
    private var listaAlumnos: List<AlumnoLista>,
    private val context: Context
) : RecyclerView.Adapter<MiAdaptador.ViewHolder>(), View.OnClickListener {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var listener: View.OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.alumno_item, parent, false)
        view.setOnClickListener(this)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alumno = listaAlumnos[position]
        holder.txtMatricula.text = alumno.matricula
        holder.txtNombre.text = alumno.nombre
        holder.txtCarrera.text = alumno.especialidad
        Glide.with(context)
            .load(alumno.foto)
            .placeholder(R.mipmap.foto)
            .error(R.mipmap.foto)
            .into(holder.idImagen)
    }



    override fun getItemCount(): Int {
        return listaAlumnos.size
    }

    fun setOnClickListener(listener: View.OnClickListener) {
        this.listener = listener
    }

    override fun onClick(v: View) {
        listener?.onClick(v)
    }

    fun actualizarLista(lista: List<AlumnoLista>) {
        listaAlumnos = lista
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtNombre: TextView = itemView.findViewById(R.id.txtNombre)
        val txtMatricula: TextView = itemView.findViewById(R.id.txtMatricula)
        val txtCarrera: TextView = itemView.findViewById(R.id.txtCarrera)
        val idImagen: ImageView = itemView.findViewById(R.id.foto)
    }
}
