package com.joseva.appmenubutton

import java.io.Serializable
data class AlumnoLista(var id:Int =0,
    var matricula : String = "",
    var nombre : String = "",
    var domicilio : String = "",
    var especialidad : String = "",
    var foto : String
) : Serializable