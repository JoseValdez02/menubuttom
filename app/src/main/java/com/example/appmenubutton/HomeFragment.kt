package com.joseva.appmenubutton

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class HomeFragment : Fragment() {
    private lateinit var imgProfile: ImageView
    private lateinit var txtNombre: TextView
    private lateinit var txtMateria: TextView
    private lateinit var txtCarrera: TextView
    private lateinit var txtCorreo : TextView
    private lateinit var txtTelefono : TextView

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        imgProfile = view.findViewById(R.id.imgProfile)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtMateria = view.findViewById(R.id.txtMateria)
        txtCarrera = view.findViewById(R.id.txtCarrera)
        txtCorreo = view.findViewById(R.id.txtCorreo)
        txtTelefono = view.findViewById(R.id.txtTelefono)


        return view
    }
}
