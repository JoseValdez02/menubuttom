package com.joseva.appmenubutton.database
import android.os.Parcel
import android.os.Parcelable

data class Alumno(
    var id: Int = 0,
    var matricula: String = "",
    var nombre: String = "",
    var domicilio: String = "",
    var especialidad: String = "",
    var foto: String = "",
)