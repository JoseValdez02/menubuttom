package com.joseva.appmenubutton.database

import android.provider.BaseColumns

class DefinirTabla {
    //clase estatica, no genera objetos, hace referencia con el nombre de la clase y atributos
    object Alumnos : BaseColumns {

        const val TABLA = "alumnos"
        const val ID = "id"
        const val MATRICULA = "matricula"
        const val NOMBRE = "nombre"
        const val DOMICILIO = "domicilio"
        const val ESPECIALIDAD = "especialidad"
        const val FOTO = "foto"

    }



}