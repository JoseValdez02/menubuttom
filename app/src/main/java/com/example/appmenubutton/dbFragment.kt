package com.joseva.appmenubutton

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.provider.MediaStore
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.joseva.appmenubutton.database.Alumno
import com.joseva.appmenubutton.database.dbAlumnos
import java.io.FileOutputStream
import java.io.IOException

class dbFragment : Fragment() {
    private lateinit var btnGuardar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var txtUrlImagen: TextView
    private lateinit var imgAlumno: ImageView
    private lateinit var db: dbAlumnos

    companion object {
        private const val IMAGE_UPLOADED = 1
        private const val REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 2
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnGuardar = view.findViewById(R.id.btnTest)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtGrado)
        txtUrlImagen = view.findViewById(R.id.lblUrlImagen)
        imgAlumno = view.findViewById(R.id.imgAlumno)

        arguments?.let {
            val alumnoLista = it.getSerializable("mialumno") as AlumnoLista
            txtNombre.setText(alumnoLista.nombre)
            txtDomicilio.setText(alumnoLista.domicilio)
            txtEspecialidad.setText(alumnoLista.especialidad)
            txtMatricula.setText(alumnoLista.matricula)
            txtUrlImagen.setText(alumnoLista.foto)
            cargarImagen(alumnoLista.foto)
        }

        db = dbAlumnos(requireContext())

        btnGuardar.setOnClickListener {
            if (Vacio()) {
                Toast.makeText(requireContext(), "Falta capturar información", Toast.LENGTH_SHORT).show()
            } else {
                val alumno = Alumno(
                    nombre = txtNombre.text.toString(),
                    matricula = txtMatricula.text.toString(),
                    domicilio = txtDomicilio.text.toString(),
                    especialidad = txtEspecialidad.text.toString(),
                    foto = txtUrlImagen.text.toString()
                )

                db.openDataBase()
                val existingAlumno = db.getAlumno(alumno.matricula)

                if (existingAlumno.id != 0) {
                    val result = db.actualizarAlumno(alumno, existingAlumno.id)
                    if (result > 0) {
                        Toast.makeText(requireContext(), "Alumno actualizado con éxito", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "Error al actualizar", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    val id: Long = db.insertarAlumno(alumno)
                    if (id > 0) {
                        Toast.makeText(requireContext(), "Alumno agregado con éxito con ID $id", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "Error al agregar", Toast.LENGTH_SHORT).show()
                    }
                }
                limpiar()
                db.close()
                (parentFragmentManager.findFragmentById(R.id.frmContenedor) as? SalirFragment)?.actualizarLista()
                parentFragmentManager.popBackStack()
            }
        }

        btnBuscar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isEmpty()) {
                Toast.makeText(requireContext(), "Debe ingresar la matrícula", Toast.LENGTH_SHORT).show()
            } else {
                db.openDataBase()
                val alumno = db.getAlumno(matricula)
                if (alumno.id != 0) {
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    txtUrlImagen.setText(alumno.foto)
                    cargarImagen(alumno.foto)
                    Toast.makeText(requireContext(), "Alumno encontrado", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                }
                db.close()
            }
        }

        imgAlumno.setOnClickListener {
            checkPermissionAndOpenImageChooser()
        }

        btnBorrar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isEmpty()) {
                Toast.makeText(requireContext(), "Por favor, ingrese la matrícula para borrar", Toast.LENGTH_SHORT).show()
            } else {
                val builder = AlertDialog.Builder(requireContext())
                builder.setMessage("¿Está seguro de eliminar este alumno?")
                    .setPositiveButton("Sí") { dialog, id ->
                        db.openDataBase()
                        val result = db.BorrarAlumno(matricula)
                        if (result > 0) {
                            Toast.makeText(requireContext(), "Alumno eliminado con éxito", Toast.LENGTH_SHORT).show()
                            limpiar()
                        } else {
                            Toast.makeText(requireContext(), "No se encontró la matrícula", Toast.LENGTH_SHORT).show()
                        }
                        db.close()
                        (parentFragmentManager.findFragmentById(R.id.frmContenedor) as? SalirFragment)?.actualizarLista()
                    }
                    .setNegativeButton("No") { dialog, id ->
                        dialog.dismiss()
                    }
                builder.create().show()
            }
        }

        return view
    }

    private fun checkPermissionAndOpenImageChooser() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_MEDIA_IMAGES
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.READ_MEDIA_IMAGES),
                    REQUEST_PERMISSION_READ_EXTERNAL_STORAGE
                )
            } else {
                openImageChooser()
            }
        } else {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_PERMISSION_READ_EXTERNAL_STORAGE
                )
            } else {
                openImageChooser()
            }
        }
    }

    private fun openImageChooser() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_UPLOADED)
    }

    private fun saveImageToInternalStorage(uri: Uri): String? {
        val bitmap: Bitmap = if (Build.VERSION.SDK_INT < 28) {
            MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, uri)
        } else {
            val source = ImageDecoder.createSource(requireActivity().contentResolver, uri)
            ImageDecoder.decodeBitmap(source)
        }

        val filename = "${System.currentTimeMillis()}.jpg"
        var fos: FileOutputStream? = null
        try {
            fos = requireActivity().openFileOutput(filename, Context.MODE_PRIVATE)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        } finally {
            fos?.close()
        }
        return requireActivity().filesDir.absolutePath + "/" + filename
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_UPLOADED && resultCode == AppCompatActivity.RESULT_OK && data != null && data.data != null) {
            val imageUri: Uri? = data.data
            try {
                imageUri?.let {
                    val imagePath = saveImageToInternalStorage(it)
                    if (imagePath != null) {
                        // Cargar imagen en ImageView y establecer la ruta en TextView
                        cargarImagen(imagePath)
                        txtUrlImagen.text = imagePath
                    } else {
                        Toast.makeText(requireContext(), "Error al guardar la imagen", Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: SecurityException) {
                Toast.makeText(requireContext(), "Permiso denegado para acceder a este URI", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL_STORAGE) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                openImageChooser()
            } else {
                Toast.makeText(requireContext(), "Permiso denegado para acceder a la galería", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun cargarImagen(filePath: String) {
        Glide.with(this)
            .load(filePath)
            .placeholder(R.mipmap.foto)
            .error(R.mipmap.foto)
            .into(imgAlumno)
    }

    private fun Vacio(): Boolean {
        return txtNombre.text.toString().isEmpty() ||
                txtDomicilio.text.toString().isEmpty() ||
                txtMatricula.text.toString().isEmpty() ||
                txtEspecialidad.text.toString().isEmpty() ||
                txtUrlImagen.text.toString().isEmpty()
    }

    private fun limpiar() {
        txtMatricula.text.clear()
        txtNombre.text.clear()
        txtDomicilio.text.clear()
        txtEspecialidad.text.clear()
        txtUrlImagen.setText("")
        imgAlumno.setImageResource(R.mipmap.foto)
    }
}

