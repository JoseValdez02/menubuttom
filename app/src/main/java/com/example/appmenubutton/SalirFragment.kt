package com.joseva.appmenubutton

import android.os.Bundle
import android.view.*
import android.widget.Filter
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.joseva.appmenubutton.database.dbAlumnos

class SalirFragment : Fragment() {
    private lateinit var rcvLista: RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var btnNuevo: FloatingActionButton
    private lateinit var searchView: SearchView
    private lateinit var listaAlumno: ArrayList<AlumnoLista>
    private lateinit var filteredList: ArrayList<AlumnoLista>
    private lateinit var db: dbAlumnos

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_salir, container, false)
        btnNuevo = view.findViewById(R.id.agregarAlumno)
        searchView = view.findViewById(R.id.menu_search)
        rcvLista = view.findViewById(R.id.recId)
        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        listaAlumno = ArrayList()
        adaptador = MiAdaptador(listaAlumno, requireContext())
        rcvLista.adapter = adaptador

        db = dbAlumnos(requireContext())
        cargarLista()

        btnNuevo.setOnClickListener {
            cambiarDBFragment()
        }

        adaptador.setOnClickListener {
            val pos :Int = rcvLista.getChildLayoutPosition(it)
            val alumno: AlumnoLista = listaAlumno[pos]

            val bundle = Bundle().apply {
                putSerializable("mialumno", alumno)
            }
            val dbFragment = dbFragment()
            dbFragment.arguments = bundle

            parentFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { filtrarAlumnos(it) }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { filtrarAlumnos(it) }
                return true
            }
        })

        return view
    }

    private fun cargarLista() {
        listaAlumno.clear()
        db.openDataBase()
        val cursor = db.getAllAlumnos()
        if (cursor.moveToFirst()) {
            do {
                val alumno = AlumnoLista(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5)
                )
                listaAlumno.add(alumno)
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()
        adaptador.notifyDataSetChanged()
    }

    private fun filtrarAlumnos(texto: String) {
        val listaFiltrada = listaAlumno.filter { alumno ->
            alumno.nombre.contains(texto, ignoreCase = true) ||
                    alumno.domicilio.contains(texto, ignoreCase = true)
        }
        adaptador.actualizarLista(listaFiltrada)
    }

    private fun cambiarDBFragment() {
        val cambioFragment = fragmentManager?.beginTransaction()
        cambioFragment?.replace(R.id.frmContenedor, dbFragment())
        cambioFragment?.addToBackStack(null)
        cambioFragment?.commit()
    }

    fun actualizarLista(){
        cargarLista()
    }
}

